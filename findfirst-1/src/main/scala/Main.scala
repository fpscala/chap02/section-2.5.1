import Finder.findFirst

object Main extends App {
  val a = Array(1, 3, 5, 7, 9)
  val idx = findFirst(a, (e: Int) => e > 5)

  printf("findFrist: %d\n", idx)
}
